import request from '@/utils/request'
// import qs from 'qs'

/**
 * 获取商品分组
 * @param {*} params
 */
export function getGoodsGroup(params) {
  return request({
    url: '/merchantCategory',
    method: 'get',
    params
  })
}
/**
 * 新增商品分组
 * @param {*} params
 */
export function postGoodsGroup(params) {
  return request({
    url: '/merchantCategory',
    method: 'POST',
    data: params
  })
}
/**
 * 修改商品分组
 * @param {*} params
 */
export function putGoodsGroup(params) {
  return request({
    url: '/merchantCategory/'+params.id,
    method: 'PUT',
    data: params
  })
}
/**
 * 修改商品分组状态
 * @param {*} params
 */
export function putGoodsGroupStatus(params) {
  return request({
    url: '/merchantCategoryStatus/'+params.id,
    method: 'PUT',
    data: params
  })
}
/**
 * 删除商品分组
 * @param {*} params
 */
export function delGoodsGroup(params) {
  return request({
    url: '/merchantCategory/'+params.id,
    method: 'DELETE',
    data: params
  })
}
/**
 * 获取商品父分组
 * @param {*} params
 */
export function getGoodsGroupParent(params) {
  return request({
    url: '/merchantCategoryParent',
    method: 'get',
    params
  })
}
/**
 * 获取区域分组
 * @param {*} params
 */
export function getGoodsArea(params) {
  return request({
    url: '/merchantGoodsCityGroup',
    method: 'get',
    params
  })
}
/**
 * 添加区域分组
 * @param {*} params
 */
export function addGoodsArea(params) {
  return request({
    url: '/merchantGoodsCityGroup',
    method: 'post',
    data: params
  })
}
/**
 * 修改区域分组
 * @param {*} params
 */
export function putGoodsArea(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantGoodsCityGroup/'+id,
    method: 'put',
    data: params
  })
}
/**
 * 获取区域数据
 * @param {*} params
 */
export function getAddr() {
  return request({
    url: '/addr',
    method: 'get',
  })
}

//商品列表
/**
 * 获取商品列表
 * @param {*} params
 */
export function getGoodsList(params) {
  return request({
    url: '/merchantGoods',
    method: 'get',
    params
  })
}
/**
 * 获取商品
 * @param {*} params
 */
export function getGoods(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantGoods/'+id,
    method: 'get',
    params
  })
}
/**
 * 修改商品
 * @param {*} params
 */
export function putGoods(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantGoods/'+id,
    method: 'put',
    data: params
  })
}
/**
 * 删除商品
 * @param {*} params
 */
export function delGoods(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantGoods/'+id,
    method: 'delete',
    data: params
  })
}
/**
 * 修改商品状态
 * @param {*} params
 */
export function putGoodsStatus(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantGood/'+id,
    method: 'put',
    data: params
  })
}
/**
 * 获取商品类目
 * @param {*} params
 */
export function getGoodsTypeSub(params) {
  return request({
    url: '/merchantCategoryTypeSub',
    method: 'get',
    params
  })
}


// 回收站
/**
 * 获取回收站列表
 * @param {*} params
 */
export function getRBlist(params) {
  return request({
    url: '/merchantGoodsRecycle',
    method: 'get',
    params
  })
}
/**
 * 恢复商品
 * @param {*} params
 */
export function recovery(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantGoodReduction/'+id,
    method: 'PUT',
    data: params
  })
}
