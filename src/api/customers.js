import request from '@/utils/request'


//团长
/**
 * 获取团长列表
 * @param {*} params 
 */
export function getTuanUserList(params) {
  return request({
    url: '/merchantTuanUser',
    method: 'get',
    params
  })
}
/**
 * 获取团长绑定商品列表
 * @param {*} params 
 */
export function getTuanGoodsList(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantLeaderGoods/'+id,
    method: 'get',
    params
  })
}
/**
 * 修改团长绑定的商品列表
 * @param {*} params 
 */
export function putTuanGoodsList(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: 'merchantLeaderGoods/'+id,
    method: 'put',
    data: params
  })
}
/**
 * 解绑会员
 * @param {*} params 
 */
export function delTuanUser(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantTuanUserUntying/'+id,
    method: 'delete',
    data: params
  })
}
/**
 * 获取团长信息
 * @param {*} params 
 */
export function getTuanUser(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantTuanUser/'+id,
    method: 'get',
    params
  })
}
/**
 * 修改团长信息
 * @param {*} params 
 */
export function putTuanUser(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantTuanUsers/'+id,
    method: 'put',
    data: params
  })
}
/**
 * 获取团长旗下团员列表
 * @param {*} params
 */
export function getTuanLeagueMemberList(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantLeagueMember/'+id,
    method: 'get',
    params
  })
}
/**
 * 保存选中的旗下团员
 * @param {*} params
 */
export function putTuanLeagueMember(params) {
  return request({
    url: '/merchantLeagueMember',
    method: 'put',
    data: params
  })
}


//团长审核
/**
 * 获取团长审核列表
 * @param {*} params 
 */
export function getTuanAuditList(params) {
  return request({
    url: '/merchantTuanUser',
    method: 'get',
    params
  })
}
/**
 * 执行团长审核
 * @param {*} params
 */
export function doTuanAudit(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantTuanUser/'+id,
    method: 'put',
    data: params
  })
}

//团长等级

/**
 * 获取团长等级列表
 * @param {*} params 
 */
export function getTuanLeaveList(params) {
  return request({
    url: '/merchantLeaderLevel',
    method: 'get',
    params
  })
}
/**
 * 添加团长等级
 * @param {*} params 
 */
export function postTuanLeave(params) {
  return request({
    url: '/merchantLeaderLevel',
    method: 'post',
    data: params
  })
}
/**
 * 修改团长等级
 * @param {*} params 
 */
export function putTuanLeave(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantLeaderLevel/'+id,
    method: 'put',
    data: params
  })
}
/**
 * 删除团长等级
 * @param {*} params 
 */
export function delTuanLeave(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantLeaderLevel/'+id,
    method: 'delete',
    data: params
  })
}


//推客
/**
 * 获取推客列表
 * @param {*} params 
 */
export function getPusherList(params) {
  return request({
    url: '/merchantTuanUser',
    method: 'get',
    params
  })
}
/**
 * 执行推客审核
 * @param {*} params
 */
export function doPusherAudit(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantTuanUser/'+id,
    method: 'put',
    data: params
  })
}

//仓库
/**
 * 获取仓库列表
 * @param {*} params 
 */
export function getWareList(params) {
  return request({
    url: '/merchantWarehouse',
    method: 'get',
    params
  })
}
/**
 * 新增仓库
 * @param {*} param 
 */
export function postWare(params){
  return request({
    url: '/merchantWarehouse',
    method: 'post',
    data: params
  })
}
/**
 * 更新仓库
 * @param {*} param 
 */
export function putWare(params){
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantWarehouse/'+id,
    method: 'put',
    data: params
  })
}
/**
 * 删除仓库
 * @param {*} param 
 */
export function delWare(params){
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantWarehouse/'+id,
    method: 'delete',
    data: params
  })
}