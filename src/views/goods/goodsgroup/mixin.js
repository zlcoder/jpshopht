import { getGoodsGroup } from '@/api/goods'
// import store from '@/store'

export default {
  data() {
    return {
      goodsGroup: [],//商品分组
      number: 0,
    }
  },
  methods: {
    /**
     * 获取商品分组列表
     */
    getGoodsGroup() {
      const params = {
        key: this.$store.state.app.activeApp.saa_key,
        limit: 10,
        page: this.page
      }
      getGoodsGroup(params).then(response => {
        if(response.status === 200){
          this.number = response.count;
          this.goodsGroup = response.data;
          this.goodsGroup.forEach(item=>{
            if(item.status==='1'){
              item.status = true;
            }else{
              item.status = false;
            }
            item.data.forEach(childItem => {
              if(childItem.status==='1'){
                childItem.status = true;
              }else{
                childItem.status = false;
              }
            })
          })
        }else{
          this.$message.error(response.msg)
        }
      })
    },
    reset() {
      this.groupData = {
        parent_id: '未选择',
        name: '',
        pic_url: '',
        img_url: '',
        status: true,
        sort: '',
        key: this.$store.state.app.activeApp.saa_key
      }
    },
  },
}