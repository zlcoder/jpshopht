
const tokens = {
  admin: {
    token: 'admin-token'
  },
  editor: {
    token: 'editor-token'
  }
}

// const users = {
//   'admin-token': {
//     roles: ['admin'],
//     introduction: 'I am a super administrator',
//     avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
//     name: 'Super Admin'
//   },
//   'editor-token': {
//     roles: ['editor'],
//     introduction: 'I am an editor',
//     avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
//     name: 'Normal Editor'
//   }
// }

export default [

  //登录
  {
    url: '/merchantlogin',
    type: 'post',

    response: config => {
      const { username, password } = config.body

      if(username == 'admin' && password == '111111'){
        return {
          status: 200,
          message: '请求成功',
          name: '15088844694',
          data: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJTSEEyNTYifQ__.eyJpYXQiOjE1NzI4MzQ2NzEsImV4cCI6MTU3Mjg3Nzg3MSwidWlkIjoiNDQzIiwic2lkIjowLCJrZXkiOiIifQ__.470287740ee9c9b31df75b893f09bd4ec32d996919f50e437727baee9e1bb1b0'
        }
      }else{
        return {
          status: 500,
          message: '账号或密码错误'
        }
      }
    }
  },

  //应用列表
  {
    url: '/merchantapp',
    type: 'get',
    response: () => {
      return {
        status: 200,
        message: '请求成功',
        data:[
          {
            saa_id: "802",
            saa_key: "000802",
            saa_name: "test3",
            category_id: "2",
            combo_id: "2",
            app_name: "社区团购微商城",
            saa_pic_url: "https://imgs.juanpao.com/merchantapp%2F2019%2F10%2F22%2F15717279005daeaa1c6a13d.png",
            combo_name: "全功能版",
            expire_time: "2021-11-25"
          },
          {
            saa_id: "802",
            saa_key: "000803",
            saa_name: "test4",
            category_id: "2",
            combo_id: "2",
            app_name: "社区团购微商城",
            saa_pic_url: "https://imgs.juanpao.com/merchantapp%2F2019%2F10%2F22%2F15717279005daeaa1c6a13d.png",
            combo_name: "全功能版",
            expire_time: "2021-11-25"
          },
          {
            saa_id: "802",
            saa_key: "000804",
            saa_name: "test5",
            category_id: "2",
            combo_id: "2",
            app_name: "社区团购微商城",
            saa_pic_url: "https://imgs.juanpao.com/merchantapp%2F2019%2F10%2F22%2F15717279005daeaa1c6a13d.png",
            combo_name: "全功能版",
            expire_time: "2021-11-25"
          },
          {
            saa_id: "802",
            saa_key: "000805",
            saa_name: "test6",
            category_id: "2",
            combo_id: "2",
            app_name: "社区团购微商城",
            saa_pic_url: "https://imgs.juanpao.com/merchantapp%2F2019%2F10%2F22%2F15717279005daeaa1c6a13d.png",
            combo_name: "全功能版",
            expire_time: "2021-11-25"
          },
          {
            saa_id: "802",
            saa_key: "000806",
            saa_name: "test7",
            category_id: "2",
            combo_id: "2",
            app_name: "社区团购微商城",
            saa_pic_url: "https://imgs.juanpao.com/merchantapp%2F2019%2F10%2F22%2F15717279005daeaa1c6a13d.png",
            combo_name: "全功能版",
            expire_time: "2021-11-25"
          },
          {
            saa_id: "802",
            saa_key: "000807",
            saa_name: "test8",
            category_id: "2",
            combo_id: "2",
            app_name: "社区团购微商城",
            saa_pic_url: "https://imgs.juanpao.com/merchantapp%2F2019%2F10%2F22%2F15717279005daeaa1c6a13d.png",
            combo_name: "全功能版",
            expire_time: "2021-11-25"
          }
        ]
      }
    }
  },

  //店铺分类列表
  {
    url: '/merchantshopcategor',
    type: 'get',
    response: () => {
      return {
        status: 200,
        message: "请求成功",
        data:
        [
          {"id":"16","name":"服装鞋包","key":"","merchant_id":"0","parent_id":"0","pic_url":"http://juanpao999-1255754174.cos.cn-south.myqcloud.com/admin%2Fshop%2Fcategory%2F15518495565c7f5854304f3.png","detail_info":"","is_top":"0","sort":"0","status":"1","create_time":"2019-02-27 16:22:36","update_time":"2019-03-06 13:19:16","delete_time":null},
          {"id":"17","name":"海淘","key":"","merchant_id":"0","parent_id":"0","pic_url":"http://juanpao999-1255754174.cos.cn-south.myqcloud.com/admin%2Fshop%2Fcategory%2F15518495615c7f58593238f.png","detail_info":"","is_top":"0","sort":"0","status":"1","create_time":"2019-02-27 16:22:59","update_time":"2019-03-06 13:19:21","delete_time":null},
          {"id":"18","name":"家居家纺","key":"","merchant_id":"0","parent_id":"0","pic_url":"http://juanpao999-1255754174.cos.cn-south.myqcloud.com/admin%2Fshop%2Fcategory%2F15517483425c7dccf65c8dd.png","detail_info":"","is_top":"0","sort":"0","status":"1","create_time":"2019-03-05 09:12:22","update_time":null,"delete_time":null},
          {"id":"19","name":"美妆洗护","key":"","merchant_id":"0","parent_id":"0","pic_url":"http://juanpao999-1255754174.cos.cn-south.myqcloud.com/admin%2Fshop%2Fcategory%2F15518347835c7f1e9fc3143.png","detail_info":"","is_top":"0","sort":"0","status":"1","create_time":"2019-03-05 09:12:43","update_time":"2019-03-06 09:13:04","delete_time":null},
          {"id":"20","name":"母婴玩具","key":"","merchant_id":"0","parent_id":"0","pic_url":"http://juanpao999-1255754174.cos.cn-south.myqcloud.com/admin%2Fshop%2Fcategory%2F15518348285c7f1ecc6b6b1.png","detail_info":"","is_top":"0","sort":"0","status":"1","create_time":"2019-03-06 09:13:48","update_time":null,"delete_time":null},
          {"id":"21","name":"汽车美容","key":"","merchant_id":"0","parent_id":"0","pic_url":"http://juanpao999-1255754174.cos.cn-south.myqcloud.com/admin%2Fshop%2Fcategory%2F15518348955c7f1f0fad856.png","detail_info":"","is_top":"0","sort":"0","status":"1","create_time":"2019-03-06 09:14:55","update_time":null,"delete_time":null},
          {"id":"22","name":"数码家电","key":"","merchant_id":"0","parent_id":"0","pic_url":"http://juanpao999-1255754174.cos.cn-south.myqcloud.com/admin%2Fshop%2Fcategory%2F15518495655c7f585dd0eeb.png","detail_info":"","is_top":"0","sort":"0","status":"1","create_time":"2019-03-06 09:15:20","update_time":"2019-03-06 13:19:25","delete_time":null},
          {"id":"23","name":"保健品","key":"","merchant_id":"0","parent_id":"0","pic_url":"http://juanpao999-1255754174.cos.cn-south.myqcloud.com/admin%2Fshop%2Fcategory%2F15518349335c7f1f35cf777.png","detail_info":"","is_top":"0","sort":"0","status":"1","create_time":"2019-03-06 09:15:34","update_time":null,"delete_time":null},
          {"id":"24","name":"珠宝饰品","key":"","merchant_id":"0","parent_id":"0","pic_url":"http://juanpao999-1255754174.cos.cn-south.myqcloud.com/admin%2Fshop%2Fcategory%2F15518349485c7f1f4412d1a.png","detail_info":"","is_top":"0","sort":"0","status":"1","create_time":"2019-03-06 09:15:48","update_time":null,"delete_time":null},
          {"id":"25","name":"眼镜手表","key":"","merchant_id":"0","parent_id":"0","pic_url":"http://juanpao999-1255754174.cos.cn-south.myqcloud.com/admin%2Fshop%2Fcategory%2F15518349565c7f1f4c52be9.png","detail_info":"","is_top":"0","sort":"0","status":"1","create_time":"2019-03-06 09:15:56","update_time":null,"delete_time":null},
          {"id":"26","name":"运动户外","key":"","merchant_id":"0","parent_id":"0","pic_url":"http://juanpao999-1255754174.cos.cn-south.myqcloud.com/admin%2Fshop%2Fcategory%2F15518349995c7f1f7795c74.png","detail_info":"","is_top":"0","sort":"0","status":"1","create_time":"2019-03-06 09:16:39","update_time":null,"delete_time":null},
          {"id":"27","name":"鲜花礼品","key":"","merchant_id":"0","parent_id":"0","pic_url":"http://juanpao999-1255754174.cos.cn-south.myqcloud.com/admin%2Fshop%2Fcategory%2F15518495355c7f583f14a3a.png","detail_info":"","is_top":"0","sort":"0","status":"1","create_time":"2019-03-06 09:18:02","update_time":"2019-03-06 13:18:55","delete_time":null},
          {"id":"28","name":"家居家纺","key":"","merchant_id":"0","parent_id":"0","pic_url":"http://juanpao999-1255754174.cos.cn-south.myqcloud.com/admin%2Fshop%2Fcategory%2F15518495395c7f5843cf36d.png","detail_info":"","is_top":"0","sort":"0","status":"1","create_time":"2019-03-06 09:18:34","update_time":"2019-03-06 13:18:59","delete_time":null},
          {"id":"29","name":"办公文具","key":"","merchant_id":"0","parent_id":"0","pic_url":"http://juanpao999-1255754174.cos.cn-south.myqcloud.com/admin%2Fshop%2Fcategory%2F15518351315c7f1ffbd4370.png","detail_info":"","is_top":"0","sort":"0","status":"1","create_time":"2019-03-06 09:18:52","update_time":null,"delete_time":null},
          {"id":"30","name":"装修建材","key":"","merchant_id":"0","parent_id":"0","pic_url":"http://juanpao999-1255754174.cos.cn-south.myqcloud.com/admin%2Fshop%2Fcategory%2F15518495445c7f58489b515.png","detail_info":"","is_top":"0","sort":"0","status":"1","create_time":"2019-03-06 09:19:16","update_time":"2019-03-06 13:19:04","delete_time":null},
          {"id":"31","name":"水果蔬菜","key":"","merchant_id":"0","parent_id":"0","pic_url":"http://juanpao999-1255754174.cos.cn-south.myqcloud.com/admin%2Fshop%2Fcategory%2F15518495495c7f584d36415.png","detail_info":"","is_top":"0","sort":"0","status":"1","create_time":"2019-03-06 09:19:44","update_time":"2019-03-06 13:19:09","delete_time":null},
          {"id":"32","name":"日用百货","key":"","merchant_id":"0","parent_id":"0","pic_url":"http://juanpao999-1255754174.cos.cn-south.myqcloud.com/admin%2Fshop%2Fcategory%2F15518352535c7f20750d1ec.png","detail_info":"","is_top":"0","sort":"0","status":"1","create_time":"2019-03-06 09:20:53","update_time":null,"delete_time":null},
          {"id":"33","name":"食品酒水","key":"","merchant_id":"0","parent_id":"0","pic_url":"http://juanpao999-1255754174.cos.cn-south.myqcloud.com/admin%2Fshop%2Fcategory%2F15518352695c7f208535153.png","detail_info":"","is_top":"0","sort":"0","status":"1","create_time":"2019-03-06 09:21:09","update_time":null,"delete_time":null}
        ],
        count: "18"
      }
    }
  },

  //创建应用
  {
    url: '/merchantpay',
    type: 'post',
    response: () => {
      const count = Math.ceil(Math.random()*10); 
      if(count>5){
        return {
          status: 200,
          message: '创建成功'
        }
      }else{
        return {
          status: 500,
          message: '您的应用已经超过限制次数'
        }
      }
    }
  },

  // user login
  {
    url: '/user/login',
    type: 'post',
    response: config => {
      const { username } = config.body
      const token = tokens[username]

      // mock error
      if (!token) {
        return {
          status: 60204,
          message: 'Account and password are incorrect.'
        }
      }

      return {
        status: 200,
        data: token
      }
    }
  },

  // get user info
  // {
  //   url: '/user/info\.*',
  //   type: 'get',
  //   response: config => {
  //     const { token } = config.query
  //     const info = users[token]

  //     // mock error
  //     if (!info) {
  //       return {
  //         status: 508,
  //         message: 'Login failed, unable to get user details.'
  //       }
  //     }

  //     return {
  //       status: 200,
  //       data: info
  //     }
  //   }
  // },

  // user logout
  {
    url: '/user/logout',
    type: 'post',
    response: () => {
      return {
        status: 200,
        data: 'success'
      }
    }
  }
]
